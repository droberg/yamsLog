1. Get protoc
 - Windows:
  * Download
 - Other:
  * Download prebuilt or source
  * (./configure, make, make install)
2. Update "protocol.proto" file in this directory

3. Generate protocol code
 - Windows:
   generate.bat
 - Other:
   ./generate.sh
4. Usage
 - Java:
  Include java/ProtbufJava.jar and generated file in ./gen/java
 - c++:
  Link to libprotoc.so (-lprotoc) and compiled file ./gen/cpp

5. Code your stuff

6. Profit!!!
